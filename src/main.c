#include "test.h"

int main() {
    if (run_all_tests()) {
        printf("PASSED");
        return 0;
    }
    printf("FAILED");
    return 1;
}
