#define _DEFAULT_SOURCE
#include "test.h"
#include <unistd.h>

#define HEAP_SIZE 10000
#define QUERY_SIZE 1000

void * heap;

static bool test0() {
	printf("Test 0: STARTED\n");
	heap = heap_init(HEAP_SIZE);
	if (heap == NULL) {
		printf("Test 0: FAILED. Heap initialization went wrong\n");
		return false;
	}
	printf("Test 0: PASSED\n");
	return true;
}


struct block_header * block0;

static bool test1() {
	printf("Test 1: STARTED\n");
	void * data = _malloc(QUERY_SIZE);
	if (data == NULL) {
		printf("Test 1: FAILED. Data is NULL\n");
		return false;
	}
	debug_heap(stdout, block0);
	if (block0->is_free != false || block0->capacity.bytes != QUERY_SIZE) {
		printf("Test 1: FAILED\n");
		return false;
	}
	
	_free(data);
	printf("Test 1: PASSED\n");
	return true;
}


static bool test2() {
	printf("Test 2: STARTED\n");
	void* data1 = _malloc(QUERY_SIZE);
	void* data2 = _malloc(QUERY_SIZE);
	void* data3 = _malloc(QUERY_SIZE);
	if ((data1 == NULL) || (data2 == NULL) || (data3==NULL)){
		printf("Test 2: FAILED\n");
		return false;
	}
	debug_heap(stdout, block0);
	_free(data1);
	debug_heap(stdout, block0);

	struct block_header *block1 = block_get_header(data1);
	struct block_header *block2 = block_get_header(data2);
	struct block_header *block3 = block_get_header(data3);
	if (block1->is_free == false || block2->is_free == true || block3->is_free == true) {
		printf("Test 2: FAILED\n");
		return false;
	}

	_free(data2);
	_free(data3);
	printf("Test 2: PASSED\n");
	return true;
}


static bool test3() {
	printf("Test 3: STARTED\n");
	void* data1 = _malloc(QUERY_SIZE);
	void* data2 = _malloc(QUERY_SIZE);
	void* data3 = _malloc(QUERY_SIZE);
	if ((data1 == NULL) || (data2 == NULL) || (data3==NULL)){
		printf("Test 3: FAILED. Data is NULL\n");
		return false;
	}

	debug_heap(stdout, block0);
	_free(data1);
	_free(data2);
	debug_heap(stdout, block0);

	struct block_header *block1 = block_get_header(data1);
	struct block_header *block2 = block_get_header(data2);
	struct block_header *block3 = block_get_header(data3);
	if (block1->is_free == false || block2->is_free == false || block3->is_free == true) {
		printf("Test 3: FAILED\n");
		return false;
	}
	_free(data3);
	printf("Test 3: PASSED\n");
	return true;
}


static bool test4() {
	printf("Test 4: STARTED\n");
	void* data1 = _malloc(HEAP_SIZE);
	void* data2 = _malloc(HEAP_SIZE);
	if ((data1 == NULL) || (data2 == NULL)){
		printf("Test 4: FAILED. Data is NULL\n");
		return false;
	}
	debug_heap(stdout, block0);

	size_t count = 0;
	struct block_header* block = block_get_header(data1);
	while(block->next != NULL) {
		block = block->next;
		count++;
	}

	if (count != 2) {
		printf("Test 4: FAILED\n");
		return false;
	}
	_free(data1);
	_free(data2);
	printf("Test 4: PASSED\n");
	return true;
}


static bool test5() {
	printf("Test 5: STARTED\n");
	struct block_header* last = block0;
	while(last->next != NULL) {
		last = last->next;
	}
	void* addr = last + last->capacity.bytes;
	mmap((uint8_t*)(getpagesize() * ((size_t) addr / getpagesize() + (((size_t) addr % getpagesize()) > 0))), 1000, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS, 0, 0);
	void* data = _malloc(3 * HEAP_SIZE);
	
	if((uint8_t*) last->next == ((uint8_t*) data - offsetof(struct block_header, contents))) {
		printf("Test 5: FAILED\n");
		return false;
	}
	
	_free(data);
	printf("Test 5: PASSED\n");
	return true;
}


bool run_all_tests() {
	if (test0()) {
		block0 = (struct block_header*) heap;
		return test1() && test2() && test3() && test4() && test5();
	}
	return false;
}
